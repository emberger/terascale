import numpy as np

primes = []
primeFactors = []
commonFactors = dict()


def Primes():

    for i in range(1, 100):
        counter = 0
        for j in range(2, i):
            if i % j == 0 and i != j:

                counter += 1
                #print("divisible by", j)
                key = j

                if key in commonFactors:
                    commonFactors[key] += 1
                else:
                    commonFactors[key] = 1
        if counter == 0:
            primes.append(i)

    print(np.amax(primes))

    print(max(zip(commonFactors.values(), commonFactors.keys())))
    tmp = 0
    for key in commonFactors:
        if tmp < key:
            tmp = key
    print(tmp)


def main():
    Primes()


if __name__ == "__main__":
    main()
